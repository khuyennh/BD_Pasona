/* Resize swap images
-------------------------------------- */
$(window).on('resize', function() {
    var win = $(this);
    if (win.width() <= 640) {
        $('img').each(function() {
            $(this).attr("src", $(this).attr("src").replace('_pc', '_sp'));
        });
    } else {
        $('img').each(function() {
            $(this).attr("src", $(this).attr("src").replace('_sp', '_pc'));
        });
    }
});

$(window).on('load', function() {
    var win = $(this);
    if (win.width() <= 640) {
        $('img').each(function() {
            $(this).attr("src", $(this).attr("src").replace('_pc', '_sp'));
        });
    } else {
        $('img').each(function() {
            $(this).attr("src", $(this).attr("src").replace('_sp', '_pc'));
        });
    }
});

/* Scroll floating menu
-------------------------------------- */
var heightHeader = $('.header_in').outerHeight();
var widthBr = $(window).width();
var heightMV = $('.local_mv').height();
if (widthBr > 640) {
    var heightMV = $('.local_mv').height();
} else {
    var heightMV = heightMV - 25;
}
$(window).scroll(function(event) {
    if ($(this).scrollTop() >= heightMV + heightHeader) {
        $('.header_in.stick').addClass('active');
    } else {
        $('.header_in.stick').removeClass('active');
    }
});
